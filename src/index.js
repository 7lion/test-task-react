import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from "react-router-dom";

import Home from './pages/Home';
import store from './redux/store';

import './assets/scss/base.scss';

ReactDOM.render(
 <Router>
   <Provider store={store}>
     <Home/>
   </Provider>
 </Router>,
 document.getElementById('root')
);
