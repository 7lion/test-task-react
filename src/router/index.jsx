import React, {Fragment} from 'react';
import {Route} from "react-router-dom";

import ResourceView from "../components/Resource/View";

const Router = () => {
  return (
   <Fragment>
     <Route path="/resource/:id" component={ResourceView} />
   </Fragment>
  );
};

export default Router;
