import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Router from '../router';
import Header from '../components/Layout/Header';
import Sidebar from '../components/Layout/Sidebar';
import {loadAppData} from '../redux/actions';

import '../assets/scss/home.scss';

const Home = () => {
  const dispatch = useDispatch();
  const {isLoaded} = useSelector(({appData}) => (appData));

  useEffect(() => dispatch(loadAppData()), [dispatch]);

  if (!isLoaded) {
    return <div>Loading...</div>;
  }

  return (
   <div className="home-page">
     <Header/>
     <div className="flex">
       <Sidebar/>
       <div className="content">
         <Router/>
       </div>
     </div>
   </div>
  );
};

export default Home;
