import * as api from './api';

export const fetchInitData = async () => {
  return api.fetchInitData().then(({resources, actions}) => {
    return {
      resources,
      actions: actions.reduce((acc, val) => ({...acc, [val.id]: val}), {})
    }
  });
};