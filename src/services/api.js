const API_URL = 'http://localhost:3000';

export const fetchInitData = async () => {
  return fetch(`${API_URL}/data.json`)
   .then((response) => response.json())
   .then((data) => data);
};
