import React from 'react';
import {useSelector} from "react-redux";
import {useParams} from "react-router-dom";

import "../../assets/scss/card.scss";

const View = () => {
  let {id} = useParams();

  const {resources, actions} = useSelector(({appData}) => (appData));
  const resource = resources.find((resource) => resource.id === id);

  if (!resource) {
    return (
     <div>Not Found!</div>
    );
  }

  return (
   <div className="card">
     <div className="card__header">{resource.name}</div>
     <div className="card__content">
       <div className="section">
         <div className="section__title">
           General Details
         </div>
         <div className="section__content">
           <div className="field">
             <div className="field__title">Name</div>
             <div className="field__value">{resource.name}</div>
           </div>
           <div className="field">
             <div className="field__title">Description</div>
             <div className="field__value">{resource.description}</div>
           </div>
           <div className="field">
             <div className="field__title">Resource Type</div>
             <div className="field__value">{resource.resourceType}</div>
           </div>
           <div className="field">
             <div className="field__title">Path</div>
             <div className="field__value">{resource.path}</div>
           </div>
         </div>
       </div>
       <div className="section">
         <div className="section__title">
           Permitted Actions
         </div>
         <div className="section__content">
           {resource.actionIds.map((id) => (
            <div key={id} className="action-item">{(actions[id] && actions[id].name) || 'N/A'}</div>
           ))}
         </div>
       </div>
     </div>
   </div>
  );
};

export default View;
