import React from 'react';
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";

const Sidebar = () => {
  const {resources} = useSelector(({appData}) => (appData));

  return (
   <div className="sidebar">
     <div className="sidebar__title">Items</div>

     <div className="sidebar-items">
       {resources.map((resource) => (
        <NavLink
         className="sidebar-item"
         activeClassName="active"
         key={resource.id}
         to={`/resource/${resource.id}`}
        >
          {resource.name}
        </NavLink>
       ))}
     </div>
   </div>
  );
};

export default Sidebar;
