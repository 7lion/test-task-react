import {
  APP_DATA_LOADED, APP_DATA_SET_ACTIONS, APP_DATA_SET_RESOURCES
} from '../actionTypes';

const initState = {
  isLoaded: false,
  actions: [],
  resources: [],
};

const ActionReducer = (state = initState, action) => {
  switch (action.type) {
    case APP_DATA_LOADED:
      return {...state, isLoaded: true};

    case APP_DATA_SET_ACTIONS:
      return {...state, actions: action.payload.actions};

    case APP_DATA_SET_RESOURCES:
      return {...state, resources: action.payload.resources};

    default:
      return state;
  }
};

export default ActionReducer;
