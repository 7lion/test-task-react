import {combineReducers} from 'redux';

import AppDataReducer from './AppDataReducer';

const reducers = combineReducers({
  appData: AppDataReducer,
});

export default reducers;
