import {fetchInitData} from '../services/appData';
import {
  APP_DATA_LOADED, APP_DATA_SET_ACTIONS, APP_DATA_SET_RESOURCES
} from '../redux/actionTypes';

export const loadAppData = () => (dispatch) => {
  fetchInitData().then(({resources, actions}) => {
    dispatch({type: APP_DATA_SET_ACTIONS, payload: {actions}});
    dispatch({type: APP_DATA_SET_RESOURCES, payload: {resources}});

    dispatch({type: APP_DATA_LOADED});
  });
};